from time import time, sleep, time_ns

from timeit import default_timer as timer

def MDC(a, b):
    maior = max(a, b)
    menor = min(a, b)
    maior_divisor = menor

    while maior_divisor != 1:
        if (maior % maior_divisor == 0) and (menor % maior_divisor == 0):
            break
        else:
            maior_divisor = maior_divisor - 1
    
    return maior_divisor

if __name__ == "__main__":
    a = int(input("Escolha o primeiro número: "))
    b = int(input("Escolha o segundo número: "))
    start_time = time()
    maximo_divisor_comum = MDC(a,b)
    print(f"O Máximo Divisor Comum entre {a} e {b} é {maximo_divisor_comum}")
    # sleep(0.1)
    final_time = time()
    tempo = (final_time - start_time)*1e3
    print("Tempo: ", tempo, final_time, start_time)