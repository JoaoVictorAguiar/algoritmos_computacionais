from time import time, sleep, time_ns

from timeit import default_timer as timer
import matplotlib.pyplot as plt
from random import randint

# def MDC_iterativo(a, b):
#     start_time = timer()
#     maior = max(a, b)
#     menor = min(a, b)
#     maior_divisor = menor

#     while maior_divisor != 1:
#         if (maior % maior_divisor == 0) and (menor % maior_divisor == 0):
#             break
#         else:
#             maior_divisor = maior_divisor - 1
    
#     tempo = round((timer() - start_time)*1e3, 3)
#     return maior_divisor, tempo

def MDC_iterativo(a, b):
    maior = max(a, b)
    menor = min(a, b)
    
    while menor != 0:
            inter = menor
            menor = maior % menor
            maior = inter
    return maior

def MDC_recursivo(a, b):
    maior = max(a, b)
    menor = min(a, b)

    if (menor == 0):
        return maior
    return MDC_recursivo(menor, maior % menor)

if __name__ == "__main__":
    tempos_1 = []
    tempos_2 = []
    quantidade = list(range(1,6))
    for i in quantidade:
        a = randint(0, 1000000)
        b = randint(0, 1000000)

        start_time = timer()
        maximo_divisor_comum_1 = MDC_iterativo(a, b)
        tempo_1 = round((timer() - start_time)*1e6, 3)
        tempos_1.append(tempo_1)
        print(f"O Máximo Divisor Comum do método iterativo entre {a} e {b} é {maximo_divisor_comum_1} e o tempo foi {tempo_1}us.")

        start_time = timer()
        maximo_divisor_comum_2 = MDC_recursivo(a, b) 
        tempo_2 = round((timer() - start_time)*1e6, 3)
        tempos_2.append(tempo_2)
        print(f"O Máximo Divisor Comum do método recursivo entre {a} e {b} é {maximo_divisor_comum_2} e o tempo foi {tempo_2}us.")
    
    plt.figure()
    plt.scatter(quantidade, tempos_1, c = 'g', label = 'Iterativo')
    plt.scatter(quantidade, tempos_2, c = 'r', label = 'Recursivo')
    plt.legend()
    plt.show()

