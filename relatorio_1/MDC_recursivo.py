def MDC(a, b):
    maior = max(a, b)
    menor = min(a, b)
    if (menor == 0):
        return maior
    else:
        return MDC(menor, maior % menor)

if __name__ == "__main__":
    a = int(input("Escolha o primeiro número: "))
    b = int(input("Escolha o segundo número: "))
    print(f"O Máximo Divisor Comum entre {a} e {b} é {MDC(a, b)}")