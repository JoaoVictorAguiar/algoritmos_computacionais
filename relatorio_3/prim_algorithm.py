import numpy as np
from copy import deepcopy
import networkx as nx
import random

vert_A = {'valor': -1, 'nome': "A", 'pai': None, 'adjacentes': [], 'matriz': 0}
vert_B = {'valor': -1, 'nome': "B", 'pai': None, 'adjacentes': [], 'matriz': 1}
vert_C = {'valor': -1, 'nome': "C", 'pai': None, 'adjacentes': [], 'matriz': 2}
vert_D = {'valor': -1, 'nome': "D", 'pai': None, 'adjacentes': [], 'matriz': 3}
vert_E = {'valor': -1, 'nome': "E", 'pai': None, 'adjacentes': [], 'matriz': 4}
vert_F = {'valor': -1, 'nome': "F", 'pai': None, 'adjacentes': [], 'matriz': 5}
vert_G = {'valor': -1, 'nome': "G", 'pai': None, 'adjacentes': [], 'matriz': 6}
vert_H = {'valor': -1, 'nome': "H", 'pai': None, 'adjacentes': [], 'matriz': 7}
vert_I = {'valor': -1, 'nome': "I", 'pai': None, 'adjacentes': [], 'matriz': 8}

def remove_min(Graph):
    menor = 99999999
    indice = -1
    for vertices in Graph:
        if(vertices['valor'] < menor):
            menor = vertices['valor']
            indice = vertices

    Graph.remove(indice)
    return indice

def edita_grafo(grafo, item):
    contador = 0
    for vertice in grafo:
        if (item['nome'] == vertice['nome']):
            grafo[contador] = item
            break
        contador += 1

def Prim(G, w, r):
    for vertice in G:
        vertice['valor'] = 99999999
        vertice['pai'] = None
    r['valor'] = 0
    Q = deepcopy(G)

    while(len(Q) != 0):
        u = remove_min(Q)
        for adjacente in u['adjacentes']:
            if((adjacente in Q) and (w[u['matriz'], adjacente['matriz']] < adjacente['valor'])):
                adjacente['pai'] = u
                adjacente['valor'] = w[u['matriz'], adjacente['matriz']]
                edita_grafo(G, adjacente)

def add_vertex_attributes(G):
    contador = 0
    for v in G.nodes:
        G.nodes[v]['valor'] = -1
        G.nodes[v]['nome'] = str(v)
        G.nodes[v]['pai'] = None
        G.nodes[v]['adjacentes'] = []
        G.nodes[v]['matriz'] = contador
        contador += 1

if __name__ == "__main__":
    Graph = [vert_A, vert_B, vert_C, vert_D, vert_E, vert_F, vert_G, vert_H]
    w = np.asarray([[ 0, 14,  10, 0,  5,  6,  0, 0],
                    [ 14, 0,  3, 0,  0,  0,  0, 0],
                    [ 10, 3,  0, 8,  0,  0,  0, 0],
                    [ 0, 0,  8, 0,  2,  0, 0, 15],
                    [ 5, 0,  0, 2,  0,  4, 9, 0],
                    [ 6, 0,  0, 0, 4, 0,  0, 0],
                    [ 0, 0,  0, 0,  9,  0,  0, 0],
                    [ 0, 0, 0, 15,  0,  0,  0, 0]])

    contador_1 = 0

    for vertice in Graph:
        contador_2 = 0
        for lateral in Graph:
            if(w[contador_1, contador_2] != 0):
                vertice['adjacentes'].append(lateral)
            contador_2 += 1
        contador_1 += 1


    Prim(Graph, w, vert_B)
    matriz_resolucao = np.zeros((len(Graph), len(Graph)))
    
    for vertice in Graph:
        if(vertice['nome'] != vert_B['nome']):
            matriz_resolucao[vertice['matriz'], vertice['pai']['matriz']] = vertice['valor']
            matriz_resolucao[vertice['pai']['matriz'], vertice['matriz']] = vertice['valor']

    print(matriz_resolucao)