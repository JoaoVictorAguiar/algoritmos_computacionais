import numpy as np
from copy import deepcopy

vert_A = {'valor': -1, 'nome': "A", 'pai': None, 'adjacentes': [], 'matriz': 0, 'vertice': 0}
vert_B = {'valor': -1, 'nome': "B", 'pai': None, 'adjacentes': [], 'matriz': 1, 'vertice': 0}
vert_C = {'valor': -1, 'nome': "C", 'pai': None, 'adjacentes': [], 'matriz': 2, 'vertice': 0}
vert_D = {'valor': -1, 'nome': "D", 'pai': None, 'adjacentes': [], 'matriz': 3, 'vertice': 0}
vert_E = {'valor': -1, 'nome': "E", 'pai': None, 'adjacentes': [], 'matriz': 4, 'vertice': 0}
vert_O = {'valor': -1, 'nome': "O", 'pai': None, 'adjacentes': [], 'matriz': 5, 'vertice': 0}
vert_T = {'valor': -1, 'nome': "T", 'pai': None, 'adjacentes': [], 'matriz': 6, 'vertice': 0}

def remove_min(Graph):
    menor = 99999999
    indice = -1
    for vertices in Graph:
        if(vertices['valor'] < menor):
            menor = vertices['valor']
            indice = vertices

    Graph.remove(indice)
    return indice

def relax(u, v, w):
    if(v['valor'] > u['valor'] + w[v['matriz'], u['matriz']]):
        v['valor'] = u['valor'] + w[v['matriz'], u['matriz']]
        v['pai'] = u
        v['vertice'] = w[v['matriz'], u['matriz']]

def edita_grafo(grafo, item):
    contador = 0
    for vertice in grafo:
        if (item['nome'] == vertice['nome']):
            grafo[contador] = item
            break
        contador += 1

def Dijkstra(G, w, s):
    for vertice in G:
        vertice['valor'] = 99999999
        vertice['pai'] = None
    s['valor'] = 0
    S = []
    Q = deepcopy(G)

    while(len(Q) != 0):
        u = remove_min(Q)
        S.append(u)
        for adjacente in u['adjacentes']:
            relax(u, adjacente, w)
            edita_grafo(G, adjacente) 

if __name__ == "__main__":
    Graph = [vert_A, vert_B, vert_C, vert_D, vert_E, vert_O, vert_T]
    w = np.asarray([[ 0, 2, 0, 7, 0, 2, 0],
                    [ 2, 0, 1, 4, 3, 5, 0],
                    [ 0, 1, 0, 0, 4, 4, 0],
                    [ 7, 4, 0, 0, 1, 0, 5],
                    [ 0, 3, 4, 1, 0, 0, 7],
                    [ 2, 5, 4, 0, 0, 0, 0],
                    [ 0, 0, 0, 5, 7, 0, 0]])
    
    contador_1 = 0

    for vertice in Graph:
        contador_2 = 0
        for lateral in Graph:
            if(w[contador_2, contador_1] != 0):
                vertice['adjacentes'].append(lateral)
            contador_2 += 1
        contador_1 += 1


    Dijkstra(Graph, w, vert_O)
    matriz_resolucao = np.zeros((len(Graph), len(Graph)))
    
    for vertice in Graph:
        if(vertice['nome'] != vert_O['nome']):
            matriz_resolucao[vertice['matriz'], vertice['pai']['matriz']] = vertice['vertice']
            matriz_resolucao[vertice['pai']['matriz'], vertice['matriz']] = vertice['vertice']

    print(matriz_resolucao)