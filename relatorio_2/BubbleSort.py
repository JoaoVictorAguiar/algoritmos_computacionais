from copy import deepcopy
from random import randint

def BubbleSort(vetor):
    for i in range(len(vetor)):
        for j in range(len(vetor) - 1, i, -1):
            if vetor[j] < vetor[j-1]:
                vetor[j], vetor[j-1] = vetor[j-1], vetor[j]

if __name__ == "__main__":
    vetor = []
    for numero in list(range(10)):
        vetor.append(randint(0, 100))
    
    print("Vetor Inicial: ", vetor)

    BubbleSort(vetor)
    print("Vetor final: ", vetor)