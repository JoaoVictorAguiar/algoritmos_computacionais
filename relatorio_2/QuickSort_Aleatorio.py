from copy import deepcopy
from random import randint

def QuickSort(A, p, r):
    if p < r:
        q = partition(A, p, r)
        QuickSort(A, p, q-1)
        QuickSort(A, q + 1, r)

def partition(A, p, r):
    random_value = randint(p, r)
    A[r], A[random_value] = A[random_value], A[r]
    x = A[r]
    i = p - 1
    for j in range(p, r):
        if A[j] <= x:
            i = i + 1
            A[i], A[j] = A[j], A[i]
    A[i+1], A[r] = A[r], A[i+1]
    return i + 1

if __name__ == "__main__":
    vetor = []

    for numero in list(range(10)):
        vetor.append(randint(0, 100))
    print("Vetor inicial: ", vetor)
    QuickSort(vetor, 0, len(vetor)-1)
    print(vetor)
