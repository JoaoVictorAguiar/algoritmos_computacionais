from timeit import default_timer as timer
import matplotlib.pyplot as plt
from random import randint
from copy import deepcopy

def BubbleSort(vetor):
    for i in range(len(vetor)):
        for j in range(len(vetor) - 1, i, -1):
            if vetor[j] < vetor[j-1]:
                vetor[j], vetor[j-1] = vetor[j-1], vetor[j]

def QuickSort_random(A, p, r):
    if p < r:
        q = random_partition(A, p, r)
        QuickSort_random(A, p, q-1)
        QuickSort_random(A, q + 1, r)

def random_partition(A, p, r):
    random_value = randint(p, r)
    A[r], A[random_value] = A[random_value], A[r]
    x = A[r]
    i = p - 1
    for j in range(p, r):
        if A[j] <= x:
            i = i + 1
            A[i], A[j] = A[j], A[i]
    A[i+1], A[r] = A[r], A[i+1]
    return i + 1

def QuickSort_first(A, p, r):
    if p < r:
        q = first_partition(A, p, r)
        QuickSort_first(A, p, q-1)
        QuickSort_first(A, q + 1, r)

def first_partition(A, p, r):
    x = A[p]
    A[p], A[r] = A[r], A[p]
    i = p - 1
    for j in range(p, r):
        if A[j] <= x:
            i = i + 1
            A[i], A[j] = A[j], A[i]
    A[i+1], A[r] = A[r], A[i+1]
    return i + 1

def QuickSort_final(A, p, r):
    if p < r:
        q = final_partition(A, p, r)
        QuickSort_final(A, p, q-1)
        QuickSort_final(A, q + 1, r)

def final_partition(A, p, r):
    x = A[r]
    i = p - 1
    for j in range(p, r):
        if A[j] <= x:
            i = i + 1
            A[i], A[j] = A[j], A[i]
    A[i+1], A[r] = A[r], A[i+1]
    return i + 1


if __name__ == "__main__":
    vetor = []
    tempos_1 = []
    tempos_2 = []
    tempos_3 = []
    iteracao = 0
    # tempos_4 = []
    for numero in list(range(500)):
        vetor.append(randint(0, 10000))

    for tamanho in vetor:
        print('Iteração: ', iteracao)
        vetor_elementos = []
        for numero in range(tamanho):
            vetor_elementos.append(randint(0, 100000))
        # print("Vetor Inicial: ", vetor_elementos)

        vetor_inter = deepcopy(vetor_elementos)
        start_time = timer()
        QuickSort_first(vetor_inter, 0, len(vetor_inter) - 1)
        tempo_1 = round((timer() - start_time), 3)
        # print("QuickSort First: ", vetor_inter)
        tempos_1.append(tempo_1)

        vetor_inter = deepcopy(vetor_elementos)
        start_time = timer()
        QuickSort_random(vetor_inter, 0, len(vetor_inter) - 1)
        tempo_2 = round((timer() - start_time), 3)
        # print("QuickSort Random: ", vetor_inter)
        tempos_2.append(tempo_2)

        vetor_inter = deepcopy(vetor_elementos)
        start_time = timer()
        BubbleSort(vetor_inter)
        tempo_3 = round((timer() - start_time), 3)
        # print("Bubble Sort: ", vetor_inter)
        tempos_3.append(tempo_3)

        # vetor_inter = deepcopy(vetor_elementos)
        # start_time = timer()
        # QuickSort_final(vetor_inter, 0, len(vetor_inter) - 1)
        # tempo_4 = round((timer() - start_time), 3)
        # # print("QuickSort First: ", vetor_inter)
        # tempos_4.append(tempo_4)

        iteracao += 1

    plt.figure()
    plt.scatter(vetor, tempos_3, label = 'Bubble Sort', color ='g')
    plt.legend()
    plt.xlabel("Tamanho do vetor")
    plt.ylabel("Tempo de execução [s]")

    plt.figure()
    plt.scatter(vetor, tempos_1, label = 'Quick Sort - First Element', color = 'r')
    plt.scatter(vetor, tempos_2, label = 'Quick Sort - Random Element', color = 'b')
    plt.legend()
    plt.xlabel("Tamanho do vetor")
    plt.ylabel("Tempo de execução [s]")

    plt.figure()
    plt.scatter(vetor, tempos_3, label = 'Bubble Sort', color ='g')
    plt.scatter(vetor, tempos_1, label = 'Quick Sort - First Element', color = 'r')
    plt.scatter(vetor, tempos_2, label = 'Quick Sort - Random Element', color = 'b')
    plt.legend()
    plt.xlabel("Tamanho do vetor")
    plt.ylabel("Tempo de execução [s]")
    plt.show()
